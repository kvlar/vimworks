# ZSH Theme

PROMPT='$(git_prompt_info)[%n@%m %{$fg[magenta]%}%2~%{$reset_color%}]$ '

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%}(%{$fg[red]%}:"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[green]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%})"

