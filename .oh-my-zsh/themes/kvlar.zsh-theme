# Based on the 'evan' theme
PROMPT='%{$reset_color%}%n@%m :: %{$fg[yellow]%}%2~%{$reset_color%}$(hg_prompt_info)$(git_prompt_info) %{$fg_bold[blue]%}%B»%b%{$reset_color%} '

ZSH_THEME_GIT_PROMPT_PREFIX=" :: %{$fg[magenta]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[magenta]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[magenta]%})"

ZSH_THEME_HG_PROMPT_PREFIX=" :: %{$fg[magenta]%}hg:(%{$fg[red]%}"
ZSH_THEME_HG_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_HG_PROMPT_DIRTY="%{$fg[magenta]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_HG_PROMPT_CLEAN="%{$fg[magenta]%})"
