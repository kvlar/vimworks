set nocompatible
execute pathogen#infect()
" Use pathogen to easily modify the runtime path to include all
" plugins under the ~/.vim/bundle directory
call pathogen#helptags()


" Edit look'n'feel
syntax on 						" color syntax
set nowrap                      " don't wrap lines
set tabstop=4                   " a tab is four spaces
set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set autoindent                  " always set autoindenting on
set copyindent                  " copy the previous indentation on autoindenting
set number                      " always show line numbers
set shiftwidth=4                " number of spaces to use for autoindenting
set shiftround                  " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch                   " set show matching parenthesis
set ignorecase                  " ignore case when searching
set smartcase                   " ignore case if search pattern is all lowercase,
                                "    case-sensitive otherwise
set expandtab
set smarttab                    " insert tabs on the start of a line according to
                                "    shiftwidth, not tabstop
set hlsearch                    " highlight search terms
set incsearch                   " show search matches as you type


" Visual style options
set t_Co=256
set background=dark
colorscheme mustang

" Misc style opts
set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set wildignore=*.swp,*.bak,*.pyc,*.class
set title                " change the terminal's title
set visualbell           " don't beep
set noerrorbells         " don't beep
set nobackup
set nowritebackup
set noswapfile


" File specyfic opts
filetype plugin indent on


" Keymaps
set pastetoggle=<C-p>
" Autosave under F2
noremap <F2> :w <CR>
inoremap <F2> <Esc>:w <CR>
" Shell under F3
map <F3> :sh<CR>

" Press F4 to toggle highlighting on/off, and show current value.
noremap <F4> :set hlsearch! hlsearch?<CR>


" Tabs
noremap <F5> :tabp <CR>
noremap <F6> :tabn <CR>
inoremap <F5> <Esc> :tabp <CR>
inoremap <F6> <Esc> :tabn <CR>

" Tags
nmap <F8> :TagbarToggle<CR> 

" Make
noremap <F12> :w <CR> :make<CR>
inoremap <F12> <Esc> :w <CR> :make <CR>

" Autocomplete
imap <C-@> <C-Space>
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <Down> pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <Up> pumvisible() ? "\<C-p>" : "\<Up>"
inoremap <expr> <C-Space> pumvisible() ? "\<C-e>" : "\<C-n>"

nnoremap <silent> <Leader>f :CommandT<CR>
nnoremap <silent> <Leader>b :CommandTBuffer<CR>

" Tabs
nnoremap <A-1> 1gt
nnoremap <A-2> 2gt
nnoremap <A-3> 3gt
nnoremap <A-4> 4gt
nnoremap <A-5> 5gt
nnoremap <A-6> 6gt
nnoremap <A-7> 7gt
nnoremap <A-8> 8gt
nnoremap <A-9> 9gt
nnoremap <A-0> 10gt

" Airlines
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline_theme = 'base16'

" NERDtree
noremap <Leader>t :NERDTreeToggle <CR> 

autocmd FileType javascript noremap <buffer> <leader>y :call JsBeautify()<cr>
" for html
autocmd FileType html noremap <buffer> <leader>y :call HtmlBeautify()<cr>
" for css or scss
autocmd FileType css noremap <buffer> <leader>y :call CSSBeautify()<cr>

autocmd FileType python nnoremap <leader>y :0,$!yapf<Cr>

" no trailing whitespaces
autocmd FileType c,cpp,java,python autocmd BufWritePre <buffer> :%s/\s\+$//e

if &term =~ '256color'
" disable Background Color Erase (BCE) so that color schemes
" render properly when inside 256-color tmux and GNU screen.
" see also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif

set complete=.,w,b,u,t

" gui mode
:set guioptions-=m  "remove menu bar
:set guioptions-=T  "remove toolbar
:set guioptions-=r  "remove right-hand scroll bar
:set guioptions-=L  "remove left-hand scroll bar

if has("gui_running")
"    colorscheme tesla
    if has("gui_gtk2")
        "colorscheme gentooish
        colorscheme atom-dark-256
        let g:airline_theme = 'luna'
        set guifont=Source\ Code\ Pro\ 10
    endif
endif






