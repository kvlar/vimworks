#!/bin/bash

git submodule update --init --recursive

cp -vR .vim ~/
cp -v .vimrc ~/
cp -v .editorconfig ~/
cp -v .tmux.conf ~/
cp -v .zshrc ~/
cp -v .oh-my-zsh/themes/*.zsh-theme $HOME/.oh-my-zsh/themes
